package train;

/**
 * Représentation d'une section de voie ferrée. C'est une sous-classe de la
 * classe {@link Element}.
 *
 * @author Fabien Dagnat <fabien.dagnat@imt-atlantique.fr>
 * @author Philippe Tanguy <philippe.tanguy@imt-atlantique.fr>
 */
public class Section extends Element {
	private boolean ocupped; 
	
	public Section(String name) {
		super(name);
		this.ocupped = false;
		
	}
	
	public synchronized void enter() {
		while(this.ocupped) {
			try {
				wait();
			}catch(InterruptedException e) {
				System.out.println(e);
			}
		}
		
		this.ocupped = true;	
		notify();
	}
	
	public synchronized Position leave(Position pos) {
		while(!this.ocupped) {
			try {
				wait();
			}catch(InterruptedException e) {
				System.out.println(e);
			}
		}		
		this.ocupped = false;		
		notify();
		return this.railway.nextElement(pos);
	}
	
	
}
