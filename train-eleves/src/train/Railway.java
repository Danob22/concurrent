package train;


/**
 * Représentation d'un circuit constitué d'éléments de voie ferrée : gare ou
 * section de voie
 * 
 * @author Fabien Dagnat <fabien.dagnat@imt-atlantique.fr>
 * @author Philippe Tanguy <philippe.tanguy@imt-atlantique.fr>
 */
import java.util.Arrays; 
public class Railway {
	private final Element[] elements;
	private boolean change;
	private int nbTrain;

	public Railway(Element[] elements) {
		if(elements == null)
			throw new NullPointerException();
		
		this.elements = elements;
		for (Element e : elements)
			e.setRailway(this);
		this.change = false;
		this.nbTrain = 0;
	}
	
	public boolean getChange() {
		return this.change;
	}
	
	public int getnbTrain() {
		return this.nbTrain;
	}
	
	
	public Element[] getElements() {
		return this.elements;
	}
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for (Element e : this.elements) {
			if (first)
				first = false;
			else
				result.append("--");
			result.append(e);
		}
		return result.toString();
	}
	
	
	public int findIndex(Element t) {
	      int i = 0; 
	      while (i < this.elements.length) { 	  
	            if (this.elements[i] == t) { 
	                return i; 
	            } 
	            else { 
	                i = i + 1; 
	            } 
	      } 
	        return -1; 
	}  
	
	public void addTrain() {
		this.nbTrain++;
	}
	public void minTrain() {
		this.nbTrain--;
	}
	
	public Position nextElement(Position currentPos) {
		Direction currentDirection = currentPos.getDic();
		int index = findIndex(currentPos.getPos());
		Position pos;
			
		if(currentDirection == Direction.LR) {
			if(index == elements.length - 1) { 
				//--------------- Current position is a stationD	
				this.change = true; // Change direction (flag)		
				pos = new Position(this.elements[index-1],  Direction.RL);				
			}else{
				pos  = new Position(this.elements[index+1],  currentDirection);		
			}
		}else {
			if(index == 0) { 
				//-------------------- Current position is a StationA
				this.change = true;	 // Change direction (flag)						
				pos  = new Position(this.elements[index+1],  Direction.LR);				
			}else {							
				pos  = new Position(this.elements[index-1],  currentDirection);	
			}
		}	
		
		//----------- ------All train in Station?
		if(this.change == true) { //Somebody changes direction
			this.minTrain();
			if(this.getnbTrain() == 0) { //There are not trains in railway track
				this.change = false;     //Everyone arrived well
			}
		}
		
		return pos;
	}
}
