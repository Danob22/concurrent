package train;

/**
 * Représentation d'une gare. C'est une sous-classe de la classe {@link Element}.
 * Une gare est caractérisée par un nom et un nombre de quais (donc de trains
 * qu'elle est susceptible d'accueillir à un instant donné).
 * 
 * @author Fabien Dagnat <fabien.dagnat@imt-atlantique.fr>
 * @author Philippe Tanguy <philippe.tanguy@imt-atlantique.fr>
 */
 
public class Station extends Element {
	private final int size;
	private int nbTrainG;

	public Station(String name, int size) {
		super(name);
		if(name == null || size <=0)
			throw new NullPointerException();
		this.size = size;
		this.nbTrainG = 0;
		
	}
	
	public synchronized void enter() {		
		while(this.nbTrainG >= this.size){
			//condition d'attente : Station is full
			try {
				wait();
			}catch(InterruptedException e) {
				System.out.println(e);
			}
		}
		this.nbTrainG++;
		notify();	
	}
	
	
	public synchronized  Position leave(Position pos) {
		while(this.nbTrainG <= 0 || (this.railway.getChange())) { 
			try {
				wait();
			}catch(InterruptedException e) {
				System.out.println(e);
			}
		}
		
		this.nbTrainG--;
		this.railway.addTrain();		
		notify();
		return this.railway.nextElement(pos);
	}
	
	
	
}
