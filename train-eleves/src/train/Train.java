package train;

/**
 * Représentation d'un train. Un train est caractérisé par deux valeurs :
 * <ol>
 *   <li>
 *     Son nom pour l'affichage.
 *   </li>
 *   <li>
 *     La position qu'il occupe dans le circuit (un élément avec une direction) : classe {@link Position}.
 *   </li>
 * </ol>
 * 
 * @author Fabien Dagnat <fabien.dagnat@imt-atlantique.fr>
 * @author Mayte segarra <mt.segarra@imt-atlantique.fr>
 * Test if the first element of a train is a station
 * @author Philippe Tanguy <philippe.tanguy@imt-atlantique.fr>
 * @version 0.3
 */
public class Train implements Runnable {
	private final String name;
	private Position pos;//final

	public Train(String name, Position p) throws BadPositionForTrainException {
		if (name == null || p == null)
			throw new NullPointerException();

		// A train should be first be in a station
		if (!(p.getPos() instanceof Station))
			throw new BadPositionForTrainException(name);

		this.name = name;
	    this.pos = p.clone();
	    
	}
	@Override
	public void run() {
		while(true) {
			try {
				this.enterElement();				
				Thread.sleep(1000);				
				this.leaveElement();
	         }  catch (InterruptedException e) {
	
	         }
		}
	}

	public void enterElement() {		
		this.pos.getPos().enter();
		System.out.println("Train["+this.name + "] --> " + this.pos);	
	}
	
	public void leaveElement() {	
		//System.out.println("<-- " + toString());
		System.out.println("Train["+this.name + "] <-- " + this.pos);
		this.pos = this.pos.getPos().leave(this.pos);
		//this.pos = this.pos.getPos().nextElement(pos);	
	
	}

	
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder("Train[");
		result.append(this.name);
		result.append("]");
		result.append(" is on ");
		result.append(this.pos);
		return result.toString();
	}
	



		
	
}
